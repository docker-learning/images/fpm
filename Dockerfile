FROM php:7.2-fpm

ENV PROJECT_USER "testapp"
ENV PROJECT_GROUP "testapp"

# Create the user system user and group
RUN groupadd -r $PROJECT_USER \
    && useradd -r -g $PROJECT_GROUP $PROJECT_USER \
    && mkdir -p /home/$PROJECT_USER \
    && chown $PROJECT_USER:$PROJECT_GROUP /home/$PROJECT_USER

RUN sed -i "s/user = www-data/user = ${PROJECT_USER}/" /usr/local/etc/php-fpm.d/www.conf \
    && sed -i "s/group = www-data/user = ${PROJECT_GROUP}/" /usr/local/etc/php-fpm.d/www.conf \
    && sed -i "s/group = www-data/user = ${PROJECT_GROUP}/" /usr/local/etc/php-fpm.d/www.conf \
    && sed -i "s/group = www-data/user = ${PROJECT_GROUP}/" /usr/local/etc/php-fpm.d/www.conf \
    && rm -rf /var/www

WORKDIR /home/$PROJECT_USER/app

COPY ./application /home/$PROJECT_USER/app
RUN chown -R $PROJECT_USER:$PORJECT_GROUP /home/$PROJECT_USER/app
